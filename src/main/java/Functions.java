public class Functions {

    public void printSumOfTwo (double a, double b) {
        System.out.println(a+b);
    }

    /*public void printSumOfTwo (int a, int b) {
        System.out.println(a+b);
    }*/

    public void divideTwoNumbers (int a, int b) {
        System.out.println(a/b);
    }

    public void divideTwoNumbers (double a, double b) {
        System.out.println(a/b);
    }

    public void calculateAverage(int... a) {
        double avg = 0;
        for(int i: a) {
            avg += i;
        }
        System.out.println(avg/a.length);
    }

    public void swap(int a, int b) {
        System.out.println(a + " " + b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println(a + " " + b);
    }

    public void evenNumber(int a) {
        System.out.println((a%2)==0);
    }

    public void printMinimum(int... a) {
        if(a.length==0) {
            System.out.println("No input");
            return;
        }
        int minimum = a[0];
        for(int i: a) {
            if(i<minimum){
                minimum=i;
            }
        }
        System.out.println(minimum);
    }


    //gia th fibonacci o arithmos a einai to synolo twn arithmwn poy ektypwnei
    public void fibonacci(int a) {
        if(a<3) {
            System.out.println("Number must be 3 or higher");
            return;
        }
        int x=0;
        int y=1;
        int z=0;
        System.out.println(x);
        System.out.println(y);
        for(int i=2; i<a; i++) {
            z=x+y;
            System.out.println(z);
            x=y;
            y=z;
        }
    }

    public void stringReversal(String a) {
        String reversed = "";
        for(int i=a.length()-1; i>=0; i--) {
            reversed += a.charAt(i);
        }
        System.out.println(reversed);
    }

    public void backwards(String a) {
        for(int i=0; i<=a.length()/2; i++) {
            if(a.charAt(i)!=a.charAt(a.length()-i-1)) {
                System.out.println("Word can't be read backwards");
                return;
            }
        }
        System.out.println("Word can be read backwards");
    }

    public void hamming(String a, String b) {
        if(a.length()!=b.length()) {
            System.out.println("Not same length of words");
            return;
        }
        int x=0;
        for(int i=0; i<a.length(); i++) {
            if(a.charAt(i)!=b.charAt(i)) {
                x+=1;
            }
        }
        System.out.println(x);
    }

    public void frequencyOfAWord(String phrase, String word) {
        String parts[] = phrase.split(" ");
        int i=0;
        for(String a: parts) {
            if(a.equalsIgnoreCase(word)) {
                i+=1;
            }
        }
        System.out.println("Times of word in phrase: " + i);


        /*int count=0;
        for(int i=0; i<=(phrase.length()-word.length()); i++) {
            String str = phrase.substring(i, i+3);
            if(str.regionMatches(true, str.length(), 0, word.length())) {
                count+=1;
            }*/
    }

    public void spaceNormalizer(String phrase) {
        phrase = phrase.replaceAll("\\s+", " ");
        System.out.println(phrase);
    }

    public void replaceSubString(String word, String newString, int a) {
        if((newString.length()+a)>word.length()) {
            System.out.println("Error");
            return;
        }
        String newWord = word.substring(0, a-1);
        newWord += newString;
        newWord += word.substring(a-1+newString.length());
        System.out.println(newWord);
    }

    public void binaryToDecimal(String binary) {
        int convertedNum=0;
        for(int i=binary.length()-1; i>=0; i--) {
            convertedNum+=(Character.getNumericValue(binary.charAt(i)))*((int) Math.pow(2, binary.length()-1-i));
        }
        System.out.println(convertedNum);
    }

    public void passwordCheck(String  pass) {
        int count=0;
        boolean upper=false;
        boolean lower=false;
        boolean digit=false;
        boolean specialChar=false;
        boolean onlyLetters=true;
        if(pass.length()<8) {
            System.out.println("Password must be at least 8 characters long");
            return;
        }
        else {
            count+=1;
        }
        for(int i=0; i<pass.length(); i++) {
            if(Character.isUpperCase(pass.charAt(i))) {
                upper=true;
            }
            if(Character.isLowerCase(pass.charAt(i))) {
                lower=true;
            }
            if(Character.isDigit(pass.charAt(i))) {
                digit=true;
            }
            if(pass.substring(i,i+1).matches("[^A-Za-z0-9 ]")) {
                specialChar=true;
            }
            if(i<pass.length()-2) {
                if(pass.substring(i,i+3).matches("^[a-zA-Z]*$")) {
                    onlyLetters=false;
                }
            }
        }
        /*for(int i=0; i<pass.length()-2; i++) {
            if(pass.substring(i,i+3).matches("^[a-zA-Z]*$")) {
                onlyLetters=false;
                break;
            }
        }*/
        if(upper) {
            count+=1;
        }
        if(lower) {
            count+=1;
        }
        if(digit) {
            count+=1;
        }
        if(specialChar) {
            count+=1;
        }
        if(onlyLetters) {
            count+=1;
        }


        if(count>3) {
            if(count<5) {
                System.out.println("password ok");
            }
            else if(count==5) {
                System.out.println("Strong password");
            }
            else {
                System.out.println("Very strong password");
            }

        }
        else {
            System.out.println("invalid password");
        }
    }


}
